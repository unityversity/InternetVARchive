# InternetVARchive lets you visit the [Internet Archive](https://archive.org/) in AR and VR, overlaying interactive info in person and providing a visitable VR version with the AR overlays accessible through the Archive web site and virtual social worlds.

About the Internet Archive
---------------------------
The Internet Archive is a 501(c)(3) non-profit library. Founded in 1996, our mission is to provide Universal Access to All Knowledge. We collect published works and make them available in digital formats. We are building a public library that can serve anyone in the world with access to the Internet.

We began in 1996 by archiving the Internet itself, a medium that was just beginning to grow in use. Like newspapers, the content published on the web was ephemeral - but unlike newspapers, no one was saving it. Today we have 20+ years of web history accessible through the Wayback Machine and we work with 450+ library and other partners through our Archive-It program to identify important web pages.

Watch a documentary film on Youtube about the Archive and see a tour of where the Internet rests its head:

[<img src="https://i.ytimg.com/vi/ec_-fgy3EGY/maxresdefault.jpg">](https://www.youtube.com/watch?v=ec_-fgy3EGY)

SCREENSHOTS
-------------
* InternetVARChive is exteremely early work in progress but it will look amazing soon with all our efforts combiend.

[<img src="https://gamebridgeu.files.wordpress.com/2017/09/internet-archive-test-1-screenshot.jpg">](https://gamebridgeu.files.wordpress.com/2017/09/internet-archive-test-1-screenshot.jpg)

LINKS
-------------
* InternetVRChive was started by the [Simbridge Hackerspaces VR](https://gitlab.com/unityversity/simbridge) team.