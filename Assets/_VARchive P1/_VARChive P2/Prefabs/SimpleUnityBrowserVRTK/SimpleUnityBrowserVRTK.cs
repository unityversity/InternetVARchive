﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleWebBrowser;

public class SimpleUnityBrowserVRTK : MonoBehaviour {

    public List<SimpleWebBrowser.WebBrowser> browsers;
    public Camera mainCamera;
	// Use this for initialization
	void Awake () {
        mainCamera = Camera.main;
        foreach (SimpleWebBrowser.WebBrowser browser in browsers)
        {
            if (mainCamera != null)
            {
                browser.MainCamera = mainCamera;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
