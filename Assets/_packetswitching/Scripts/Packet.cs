﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Packet : MonoBehaviour {

	public float speed = 5;
	public float orbitRadius = 0.6f;
	float orbitSwerve = 0.5f;
	Rigidbody rb;
	public ServerNode center;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		center.packets.Add(this);
	}
	GameObject line_dir;
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 v = rb.velocity;
		float s = v.magnitude;
		if(s == 0){
			v = Random.onUnitSphere;
		}

		if(center) {
			//v += Random.onUnitSphere * orbitSwerve;
			Vector3 delta = transform.position - center.transform.position;
			float d = delta.magnitude;
			Vector3 normal = delta / d;
			Vector3 idealPosition = center.transform.position + normal * orbitRadius;
			Vector3 towardIdeal = idealPosition - transform.position;
			Vector3 towardCenter = normal * Vector3.Dot(v, normal);
			v -= towardCenter;
			v += towardIdeal;
		}
		rb.velocity = v.normalized * speed;
	}

	public void SwitchTo(ServerNode sn) {
		transform.SetParent(null);
		center.packets.Remove(this);
		center = sn;
		sn.packets.Add(this);
		transform.SetParent(sn.transform);
	}

}
