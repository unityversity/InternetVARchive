﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeGenerator : MonoBehaviour {

	public GameObject prefabNode;
	public int count = 50;
	// Use this for initialization
	void Start () {
		BoxCollider bc = GetComponent<BoxCollider>();
		for(int i = 0; i < count; ++i) {
			Vector3 p = new Vector3(
				Random.RandomRange(bc.bounds.min.x,bc.bounds.max.x),
				Random.RandomRange(bc.bounds.min.y,bc.bounds.max.y),
				Random.RandomRange(bc.bounds.min.z,bc.bounds.max.z));
			GameObject node = Instantiate(prefabNode, p, Quaternion.identity);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
