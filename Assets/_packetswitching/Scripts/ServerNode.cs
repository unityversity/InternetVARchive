﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerNode : MonoBehaviour {

	public List<ServerNode> neighbors = new List<ServerNode>();
	public List<Packet> packets = new List<Packet>();
	Rigidbody rb;
	SphereCollider sc;
	public float communityForce = 0.125f;
	public float spreadForce = 1;
	float timer;
	public float switchLag = 1;
	void Start () {
		rb = GetComponent<Rigidbody>();
		sc = GetComponent<SphereCollider>();
		timer += Random.Range(0, switchLag);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 pos = Vector3.zero;
		for(int i=0; i < neighbors.Count; ++i) {
			pos += neighbors[i].transform.position;
		}
		pos /= neighbors.Count;
		Vector3 delta = pos-transform.position;
		rb.velocity += delta.normalized * communityForce * Time.deltaTime;

		timer += Time.deltaTime;
		if(timer >= switchLag) {
			timer -= switchLag;
			if(packets.Count > 1 && neighbors.Count > 0) {
				int neighbor = (int)Random.Range(0, neighbors.Count);
				Debug.Log(neighbor);
				packets[0].SwitchTo(neighbors[neighbor]);
			}
		}
	}
	void OnTriggerStay(Collider c) {
		ServerNode other = c.GetComponent<ServerNode>();
		if(other != null && c.gameObject != gameObject) {
			Vector3 delta = transform.position - c.transform.position;
			Vector3 dir = delta.normalized;
			rb.velocity += dir * Time.deltaTime * spreadForce;
			if(neighbors.IndexOf(other) < 0) {
				neighbors.Add(other);
			}
		}
	}

}
