﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wayback : MonoBehaviour {



    [System.Serializable]
    public class WaybackPoint
    {
        public string name = "";
        public System.DateTime dateTime;
        public 
            GameObject[] gameObjects;
    }

    public List<WaybackPoint> waybackPoints;

    public int currentWaybackIndex = 0;

    public KeyCode nextKC;
    public KeyCode prevKC;

    public bool hideAllWaypointsOnStart = true;

    public void DoActivateTrigger()
    {
        WayForward();
    }

    public void WayForward()
    {
        currentWaybackIndex++;
        if (currentWaybackIndex > waybackPoints.Count - 1)
        {
            currentWaybackIndex = waybackPoints.Count - 1;
        }
        UpdateWaybackPoint();
    }

    public void WayBack()
    {
        currentWaybackIndex--;
        if (currentWaybackIndex < 0)
        {
            currentWaybackIndex = 0;
        }
        UpdateWaybackPoint();
    }

	// Use this for initialization
	void Start () {
		if (hideAllWaypointsOnStart)
        {
            foreach (WaybackPoint point in waybackPoints)
            {
                foreach (GameObject go in point.gameObjects)
                {
                    go.SetActive(false);
                }
            }
        }
	}
	

	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.Backspace))
            {
                Application.LoadLevel(Application.loadedLevel);
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
            if (Input.GetKeyDown(nextKC))
            {
                WayForward();
            }
            if (Input.GetKeyDown(prevKC))
            {
                WayBack();
            }
        }

	}

    public void UpdateWaybackPoint()
    {
        foreach (GameObject theWP in waybackPoints[currentWaybackIndex].gameObjects)
        {
            theWP.SetActive(true);

        }
    }
}
